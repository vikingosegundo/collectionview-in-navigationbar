//
//  NavigationBar.h
//  NavBarCollectionView
//
//  Created by Manuel Meyer on 03.04.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationBar : UINavigationBar
@property (nonatomic) BOOL increasedHeight;
@end
