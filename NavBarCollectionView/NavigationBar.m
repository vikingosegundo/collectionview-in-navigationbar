//
//  NavigationBar.m
//  NavBarCollectionView
//
//  Created by Manuel Meyer on 03.04.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import "NavigationBar.h"

@implementation NavigationBar

- (CGSize)sizeThatFits:(CGSize)size
{
    if (self.increasedHeight)
        return CGSizeMake(self.superview.bounds.size.width, 80);
    return [super sizeThatFits:CGSizeMake(self.superview.bounds.size.width, 44)];
}

-(void)setFrame:(CGRect)frame {
    if (self.increasedHeight) {
        CGRect f = frame;
        f.size.height = 80;
        [super setFrame:f];
    } else {
        CGRect f = frame;
        f.size.height = 44;
        [super setFrame:f];
    }
}

-(void)setIncreasedHeight:(BOOL)increasedHeight
{
    _increasedHeight = increasedHeight;
    [self setFrame:self.frame];
}

@end
