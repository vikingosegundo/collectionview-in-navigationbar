//
//  SecondViewController.m
//  NavBarCollectionView
//
//  Created by Manuel Meyer on 03.04.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import "SecondViewController.h"
#import "CollectionViewCell.h"
#import "NavigationBar.h"

@interface SecondViewController () <UICollectionViewDelegate, UICollectionViewDataSource>


@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 300, 80) collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = collectionView;
    [collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [((NavigationBar *)self.navigationController.navigationBar) setIncreasedHeight:YES];

    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [((NavigationBar *)self.navigationController.navigationBar) setIncreasedHeight:NO];

    [super viewWillDisappear:animated];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor orangeColor];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 50);
}

@end
