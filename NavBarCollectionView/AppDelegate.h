//
//  AppDelegate.h
//  NavBarCollectionView
//
//  Created by Manuel Meyer on 03.04.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

