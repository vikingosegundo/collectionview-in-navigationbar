//
//  ViewController.m
//  NavBarCollectionView
//
//  Created by Manuel Meyer on 03.04.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import "ViewController.h"
#import "NavigationBar.h"
#import "SecondViewController.h"

@interface ViewController ()
@property (nonatomic, weak) UICollectionView *collectionView;
@end

@implementation ViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SecondViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    });


}


@end
